# Go talks

Here you can find some slides for a couple of Go talks.

## Running the slides

### Using Docker

1. Install [Docker](https://docs.docker.com/install/)

2. Install [Docker Compose](https://docs.docker.com/compose/install/)

3. Clone this repo

    ```
    git clone https://gitlab.com/zareone/gotalks.git
    ```

4. Run the slides:

    ```
    cd gotalks
    docker-compose up
    ```

5. Open your browser to the IP/port specified by the Present tool (most likely [http://localhost:3999](http://localhost:3999))

### Using Go on local machine

1. Install [Go](http://golang.org/doc/install) for your platform if needed.

2. Install the *Present* tool:

    ```
    go get golang.org/x/tools/cmd/present
    ```

3. Clone this repo

    ```
    git clone https://gitlab.com/zareone/gotalks.git
    ```

4. Run the Present tool in the repo folder

    ```
    cd gotalks
    present -http=:3999 -orighost=localhost -notes
    ```

5. Open your browser to the IP/port specified by the Present tool (most likely [http://localhost:3999](http://localhost:3999))
