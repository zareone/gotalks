package main

import "fmt"

func main() {
	// START OMIT
	outOfRange := func(items []string) {
		// típico error "off by one"...
		for n := 0; n < len(items)+1; n++ { // HL
			item := items[n]
			fmt.Printf("item at index %d: %s\n", n, item)
		}
	}

	func() {
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("recovered from panic: %v\n", err)
			}
		}()
		outOfRange([]string{"laptop", "mouse", "headphones", "monitor"})
	}()

	fmt.Println("program finished in a controlled way.")
	// END OMIT
}
