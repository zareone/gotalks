package main

import (
	"fmt"
	"math/rand"
	"time"
)

func sleepRand(who string) {
	t := time.Duration(rand.Intn(1000)) * time.Millisecond
	fmt.Printf("%s sleeping for %v\n", who, t)
	time.Sleep(t)
}

func main() {

	// START OMIT
	pinger := func(ch chan string, done chan bool) {
		for i := 0; i < 5; i++ {
			ch <- "Ping!" // enviamos el string por el channel
			msg := <-ch   // esperamos hasta recibir un mensaje
			fmt.Printf("Pinger: Message received: %s\n", msg)
			sleepRand("Pinger") // esperamos
		}
		close(ch)
		done <- true // enviamos en el channel, para finalizar el programa
	}

	ponger := func(ch chan string) {
		for msg := range ch { // iteramos por los mensajes entrantes
			fmt.Printf("Ponger: Message received: %s\n", msg)
			sleepRand("Ponger")
			ch <- "Pong!"
		}
	}
	// MID OMIT

	done := make(chan bool)
	pingPong := make(chan string)

	go pinger(pingPong, done)
	go ponger(pingPong)

	<-done // bloqueamos hasta que entre un mensaje en el channel

	// END OMIT
}
