package main

import "fmt"

func main() {

	// START OMIT
	fn := func() {
		fmt.Println("Function start")
		defer fmt.Println("PHP")
		defer fmt.Println("Go")
		defer fmt.Println("Rust")
		fmt.Println("Function end")
	}

	fmt.Println("before call")
	fn()
	fmt.Println("after call")
	// END OMIT
}
