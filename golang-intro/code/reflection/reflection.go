package main

import (
	"errors"
	"fmt"
	"reflect"
)

// START OMIT
type User struct {
	ID    uint
	Name  string
	Email string
}

func enumerateStructFields(i interface{}) error {
	val := reflect.ValueOf(i)
	if val.Kind() != reflect.Struct {
		return errors.New("enumerateStructFields: value must be a struct")
	}
	for n := 0; n < val.NumField(); n++ {
		field := val.Field(n)
		fieldType := val.Type().Field(n)
		fmt.Printf("Field '%s' has type '%s' and value '%v'\n", fieldType.Name, fieldType.Type, field.Interface())
	}
	return nil
}

func main() {
	fmt.Println("User struct, error:", enumerateStructFields(
		User{ID: 909, Name: "Ricardo", Email: "ricardo@borriquero.es"}))
	fmt.Println("int 43, error: ", enumerateStructFields(43))
}

// END OMIT
