package main

import (
	"fmt"
	"math/rand"
	"time"
)

// START OMIT
func main() {
	textFunc := func(text string) {
		for {
			fmt.Println(text)
			pause := rand.Intn(5)
			time.Sleep(time.Duration(pause) * time.Second)
		}
	}

	go textFunc("Oh!")   // lanzamos una goroutine
	go textFunc("Yeah!") // otra goroutine

	time.Sleep(30 * time.Second) // si no esperamos, el programa acabaría inmediatamente
}

// END OMIT
