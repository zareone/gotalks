package main

import "fmt"

func main() {
	// START OMIT
	brands := map[string]int{
		"ARRI":               1917,
		"Sony":               1946,
		"RED Digital Cinema": 1999,
		"Blackmagic Design":  2001,
	}
	for company, year := range brands {
		fmt.Printf("Company %s was founded in %d\n", company, year)
	}

	planets := []string{
		"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune",
	}
	for _, planet := range planets { // usamos _ porque no interesa el index
		fmt.Println(planet)
	}
	// END OMIT
}
