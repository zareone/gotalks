package main

import (
	"fmt"
)

func main() {
	var tShirtPrice int
	// START OMIT
	switch {
	case tShirtPrice < 0:
		fmt.Printf("wat?!")
	case tShirtPrice < 10:
		fmt.Println("Cheap!")
	case tShirtPrice < 50:
		fmt.Println("Meh")
	case tShirtPrice < 100:
		fmt.Println("Ouch!")
	default:
		fmt.Println("No way...")
	}
	// END OMIT
}
