package main

import "fmt"

// START OMIT
type User struct {
	Name      string
	Age       uint16
	Interests []string
}

func (u User) String() string {
	return fmt.Sprintf(
		"%s has %d years, and is interested in\n%v", // string (formato)
		u.Name,      // string
		u.Age,       // uint16
		u.Interests, // []string
	)
}

func main() {
	u := User{Name: "Ernesto", Age: 23, Interests: []string{"Rust", "Boxing", "Movies"}}
	// User implementa el interfaz Stringer! así que puede ser convertido a string (%s)
	fmt.Printf("%s", u)
}

// END OMIT
