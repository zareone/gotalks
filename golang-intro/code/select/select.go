package main

import (
	"fmt"
	"time"
)

func main() {

	// START OMIT
	timeout := time.After(10 * time.Second)
	ticker := time.NewTicker(time.Second)

	var tick bool
	for { // bucle "infinito"
		select {
		case <-ticker.C:
			tick = !tick
			if tick {
				fmt.Println("tick...")
			} else {
				fmt.Println("tock...")
			}

		case <-timeout:
			fmt.Println("It's time to Go!")
			return
		}
	}
	// END OMIT
}
