package log

import "fmt"

// START OMIT
type Logger struct{}

func (l *Logger) Debug(format string, args ...interface{}) error {
	line := l.formatLine(format, args...)
	_, err := fmt.Println(line) // HL
	return err
}

// END OMIT

func (l *Logger) formatLine(format string, args ...interface{}) string {
	return ""
}
