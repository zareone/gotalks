package main

import (
	"fmt"
	"math/rand"
)

func main() {

	// START OMIT
	// for clásico: incialización, condición, sentencia post-condición
	for n := 0; n < 5; n++ {
		fmt.Printf("n value is %d\n", n)
	}

	condition := true
	// podemos prescindir de la inicialización y post-condición
	// equivalente a while(condition) en otros lenguajes
	for condition {
		num := rand.Int()
		fmt.Printf("rand num %d\n", num)
		if num%13 == 0 {
			condition = false
		}
	}
	// END OMIT
}
