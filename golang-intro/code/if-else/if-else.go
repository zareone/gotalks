package main

func doSomething() {}

func doSomethingElse() {}

func removeFile(f string) error { return nil }

func main() {
	var someCondition bool
	var someOtherCondition bool

	// START OMIT
	// la condición no usa paréntesis
	if someCondition { // HL
		doSomething()
	}

	if someCondition {
		return
	} else if someOtherCondition {
		doSomething()
	} else {
		doSomethingElse()
	}

	// es posible ejecutar una sentencia antes de la evaluación
	if err := removeFile("someFile.txt"); err != nil { // HL
		doSomething()
	}
	// END OMIT

}
