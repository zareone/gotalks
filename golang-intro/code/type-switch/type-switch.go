package main

import (
	"fmt"
	"strconv"
)

// START_FUNC OMIT
func toString(v interface{}) (string, error) {
	switch t := v.(type) {
	case fmt.Stringer:
		return t.String(), nil
	case string:
		return t, nil
	case int:
		return strconv.Itoa(t), nil
	default:
		return "", fmt.Errorf("ERROR! unknown type %T", v)
	}
}

// END_FUNC OMIT

type Distance float64

func (d Distance) String() string { return fmt.Sprintf("%.2f Km", d) }

func main() {
	fmt.Println(toString([]string{"one", "two"}))
	fmt.Println(toString("already a string..."))
	fmt.Println(toString(Distance(165.32)))
	fmt.Println(toString(16))
}

// END_MAIN OMIT
