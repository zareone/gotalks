package main

import (
	"fmt"
	"os"
)

func main() {
	var severity string
	var errorMessage string
	// START OMIT
	switch severity {
	case "low":
		fallthrough // HL
	case "medium":
		fmt.Printf("an error has been detected: %s", errorMessage)
	case "critical":
		fmt.Printf("a critical error has been detected: %s. Exiting...", errorMessage)
		os.Exit(808)
	}
	// END OMIT
}
