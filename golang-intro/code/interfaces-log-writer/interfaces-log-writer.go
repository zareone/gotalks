package log

import (
	"io"
)

// START OMIT
type Logger struct {
	w io.Writer // HL
}

func NewLogger(w io.Writer) *Logger {
	return &Logger{
		w: w,
	}
}

func (l *Logger) Debug(format string, args ...interface{}) error {
	line := l.formatLine(format, args...)
	_, err := l.w.Write([]byte(line)) // HL

	return err
}

// END OMIT

func (l *Logger) formatLine(format string, args ...interface{}) string {
	return ""
}
